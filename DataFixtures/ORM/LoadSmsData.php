<?php
namespace Nitra\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Nitra\SMSBundle\Entity\Sms;

/**
 * LoadSmsData
 */
class LoadSmsData extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; 
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        
        // создать смс 
        for($i=10; $i<=50; $i++) {
            // создать
            $sms = new Sms();
            $sms->setPhone('3806764900'.$i)
                ->setMessage('Message DataFixtures Sms '.$i)
                ;
            $manager->persist($sms);
        }
        
        // сохранить 
        $manager->flush();
    }
    
}
