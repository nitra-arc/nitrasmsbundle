<?php

namespace Nitra\SMSBundle\Controller\Sms;

use Admingenerated\NitraSMSBundle\BaseSmsController\ListController as BaseListController;

/**
 * ListController
 */
class ListController extends BaseListController
{

    /**
     * processFilters
     * @param $query
     */
    protected function processFilters($query)
    {

        // обработать фильтр родителем
        parent::processFilters($query);

        // получить сохраненные фильтры
        $filters = $this->getFilters();

        // получить фильры запроса
        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        // фильтр  
        if (
        // телефон 
                (isset($filters['phone']) && $filters['phone'] !== null)
        ) {

            // добавить фильтр по номеру телефон
            if (isset($filters['phone']) && $filters['phone'] !== null) {
                $smsPhone = str_replace(array('+', '-', '(', ')', ' ', '_'), '', $filters['phone']);
                $query
                        ->andWhere('q.phone LIKE :phone')
                        ->setParameter('phone', '%' . $smsPhone . '%');
            }
        }
    }

}
