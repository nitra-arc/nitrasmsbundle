<?php

namespace Nitra\SMSBundle\Controller\Sms;

use Admingenerated\NitraSMSBundle\BaseSmsController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * EditController
 */
class EditController extends BaseEditController
{
    
    /**
     * Редактировать отправленное sms-сообщение
     */
    public function indexAction($pk)
    {
        // перенаправить пользователя на просмотр sms-сообщения
        return new RedirectResponse($this->generateUrl('Nitra_SMSBundle_Sms_show', array('pk' => $pk) ));
    }    
}
