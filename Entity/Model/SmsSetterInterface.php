<?php

namespace Nitra\SMSBundle\Entity\Model;

/**
 * SmsSetterInterface
 */
interface SmsSetterInterface
{

    /**
     * Set phone
     * @param string $phone
     * @return Sms
     */
    public function setPhone($phone);

    /**
     * Set message
     * @param string $message
     * @return Sms
     */
    public function setMessage($message);

    /**
     * Set cost
     * @param string $cost
     * @return Sms
     */
    public function setCost($cost);
    
}
