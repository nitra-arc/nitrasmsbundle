<?php

namespace Nitra\SMSBundle\Entity\Model;

/**
 * SmsInterface
 */
interface SmsInterface extends SmsSetterInterface
{

    /**
     * Get id
     * @return id $id
     */
    public function getId();

    /**
     * this object to string
     * @return string
     */
    public function __toString();

    /**
     * Получить номер телефона для отображения
     * @return string 
     */
    public function getPhoneDisplay();

    /**
     * Get phone
     * @return string 
     */
    public function getPhone();

    /**
     * Get message
     * @return string 
     */
    public function getMessage();

    /**
     * Get cost
     * @return string 
     */
    public function getCost();
    
}
