<?php

namespace Nitra\SMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\SMSBundle\Entity\Sms
 * @ORM\MappedSuperclass()
 */
class Sms implements Model\SmsInterface
{

    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $phone
     * @ORM\Column( type="string", length=15)
     * @Assert\NotBlank(message="Не указан телефон")
     * @Assert\Length(max="15")
     */
    protected $phone;

    /**
     * @var string $message
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Не указан текст сообщения")
     */
    protected $message;

    /**
     * @var float $cost
     * @ORM\Column(type="decimal", precision=2, scale=2, nullable=true)
     */
    protected $cost;

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getMessage();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPhoneDisplay()
    {
        // в зависимости от кол-ва цифр в номере телефона 
        // меняем формат отоюражения
        switch (strlen($this->phone)) {

            // россия
            case 11:
                return preg_replace("/([0-9{1})([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{2})/", "$1 ($2) $3-$4-$5", $this->phone);
            // украина 
            case 12:
                return preg_replace("/([0-9{1})([0-9]{2})([0-9]{3})([0-9]{3})([0-9]{2})/", "$1 ($2) $3-$4-$5", $this->phone);
                break;
        }

        // вернуть телефор без формата
        return $this->phone;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCost()
    {
        return $this->cost;
    }

}
