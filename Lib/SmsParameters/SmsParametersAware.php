<?php

namespace Nitra\SMSBundle\Lib\SmsParameters;

/**
 * SmsParametersAware
 *
 */
abstract class SmsParametersAware implements SmsParametersAwareInterface
{
    
    /**
     * @var \Nitra\SMSBundle\Lib\SmsParameters\SmsParameters
     */
    protected $smsParameters;
    
    /**
     * {@inheritdoc}
     */
    public function getSmsParameters()
    {
        return $this->smsParameters;
    }
    
}
