<?php

namespace Nitra\SMSBundle\Lib\SmsParameters;

/**
 * SmsParametersInterface
 * Реализация параметры sms-сообщений
 */
interface SmsParametersInterface
{
    
    /**
     * Получить массив доступных окружений в которых отправляются sms-сообщения
     * @return array
     */
    public function getAllowEnvironments();
    
    /**
     * Получить массив форматов sms сообщений
     * @return array
     */
    public function getFormats();

    /**
     * получить Entity используемый интегрой
     * @param  string $name - ключь-название Entity
     * @return string Entity
     */
    public function getEntity($name);
    
    /**
     * Returns the parameters.
     * @return array An array of parameters
     * @api
     */
    public function all();

    /**
     * Returns the parameter keys.
     * @return array An array of parameter keys
     * @api
     */
    public function keys();

    /**
     * Replaces the current parameters by a new set.
     * @param array $parameters An array of parameters
     * @api
     */
    public function replace(array $parameters = array());

    /**
     * Adds parameters.
     * @param array $parameters An array of parameters
     * @api
     */
    public function add(array $parameters = array());

    /**
     * Returns a parameter by name.
     * @param string  $path    The key
     * @param mixed   $default The default value if the parameter key does not exist
     * @param bool    $deep    If true, a path like foo[bar] will find deeper items
     * @return mixed
     * @throws \InvalidArgumentException
     * @api
     */
    public function get($path, $default = null, $deep = false);

    /**
     * Sets a parameter by name.
     * @param string $key   The key
     * @param mixed  $value The value
     * @api
     */
    public function set($key, $value);

    /**
     * Returns true if the parameter is defined.
     * @param string $key The key
     * @return bool    true if the parameter exists, false otherwise
     * @api
     */
    public function has($key);

    /**
     * Removes a parameter.
     * @param string $key The key
     * @api
     */
    public function remove($key);
    
}
