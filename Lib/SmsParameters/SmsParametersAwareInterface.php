<?php

namespace Nitra\SMSBundle\Lib\SmsParameters;

/**
 * SmsParametersAwareInterface
 * Реализация получения sms-сообщений
 */
interface SmsParametersAwareInterface
{
    /**
     * Получить паракметры sms-сообщений
     * @return \Nitra\SMSBundle\Lib\SmsParameters\SmsParameters
     */
    public function getSmsParameters();
    
}
