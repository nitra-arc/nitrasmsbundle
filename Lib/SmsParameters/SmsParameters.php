<?php

namespace Nitra\SMSBundle\Lib\SmsParameters;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * SmsParameters
 * Параметры sms-сообщений
 */
class SmsParameters extends ParameterBag implements SmsParametersInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function getAllowEnvironments()
    {
        // получить значение параметра
        $value = parent::get('allow_environments');
        
        // проверить значение параметра
        // если запрашиваемый параметр не найден
        if (!$value) {
            throw new \Exception('Параметр SMSBundle allow_environments: не найден.');
        }
        
        // вернуть форматы
        return $value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormats()
    {
        // получить значение параметра
        $value = parent::get('formats');
        
        // проверить значение параметра
        // если запрашиваемый параметр не найден
        if (!$value) {
            throw new \Exception('Параметр SMSBundle formats: не найден.');
        }
        
        // вернуть форматы
        return $value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEntity($name)
    {
        // получить значение параметра
        $value = parent::get('entity[' . $name . ']', null, true);
        
        // проверить значение параметра
        // если запрашиваемый параметр не найден
        if (!$value) {
            throw new \Exception('Параметр SMSBundle Entity: "' . $name . '" не найден.');
        }
        
        // вернуть Entity
        return $value;
    }

}
