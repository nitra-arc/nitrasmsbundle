<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\Processor;

use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersAwareInterface;

/**
 * ProcessorAbstractInterface
 * абстрактный сервис отправки sms-сообщений
 */
interface ProcessorAbstractInterface extends SmsParametersAwareInterface
{
    /**
     * получить EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager();
    
    /**
     * Получить валидатор
     * @return \Symfony\Component\Validator\Validator
     */
    public function getValidator();
    
    /**
     * Получить управляющий класс 
     * @return \Nitra\SMSBundle\Lib\SmsFactory\SmsFactory
     */
    public function getSmsFactory();
    
}
