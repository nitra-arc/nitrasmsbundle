<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\Processor;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Validator;
use Nitra\SMSBundle\Lib\SmsFactory\SmsFactory;
use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParameters;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersAware;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersInterface;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendSuccessInterface;

/**
 * ProcessorAbstract
 * Определение sms-сервиса
 */
abstract class ProcessorAbstract extends SmsParametersAware implements ProcessorAbstractInterface, ProcessorInterface
{
    
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    /**
     * @var \Symfony\Component\Validator\Validator $validator
     */
    protected $validator;
    
    /**
     * @var \Nitra\SMSBundle\Lib\SmsFactory\SmsFactory $smsFactory
     */
    protected $smsFactory;

    /**
     * получить баланс
     * @return string - баланс в виде строки
     *          false - ошибка
     */
    abstract public function getBalance();
    
    /**
     * {@inheritdoc}
     */
    abstract public function buildSms(SmsBuilderInterface $smsBuilder /*, SmsMessageInterface $smsMessage/*, SmsParametersInterface $smsParameters*/);
    
    /**
     * {@inheritdoc}
     */
    abstract public function getNewSms();
    
    /**
     * {@inheritdoc}
     */
    abstract public function validate(SmsMessageInterface $smsMessage);
    
    /**
     * {@inheritdoc}
     */
    abstract  public function send(SmsMessageInterface $smsMessage, $addLog = true);
    
    /**
     * {@inheritdoc}
     */
    abstract public function trySend(SmsMessageInterface $smsMessage, $addLog = true);

    /**
     * {@inheritdoc}
     */
    abstract public function addLog(SmsMessageInterface $smsMessage, SmsSendSuccessInterface $smsSend);
    
    /**
     * Отправить запрос на сервер
     * @param string $command - название команды
     * @param array  $options - массив параметров передаваемых на сервер SMS центра
     * @return array - ответ сервера
     */
    abstract protected function apiSendRequest($command, array $options=null);
    
    /**
     * Конструктор
     * @param \Doctrine\ORM\EntityManager $em EntityManagerы
     * @param \Symfony\Component\Validator\Validator $validator - валидатор
     * @param \Nitra\SMSBundle\Lib\SmsFactory\SmsFactory $smsFactory
     * @param \Nitra\SMSBundle\Lib\SmsParameters\SmsParameters $smsParameters
     */
    public function __construct(
        EntityManager   $em,
        Validator       $validator,
        SmsFactory      $smsFactory,
        SmsParameters   $smsParameters
    ) {
        // установить зависимости
        $this->em               = $em;
        $this->validator        = $validator;
        $this->smsFactory       = $smsFactory;
        $this->smsParameters    = $smsParameters;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEntityManager()
    {
        return $this->em;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getValidator()
    {
        return $this->validator;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSmsFactory()
    {
        return $this->smsFactory;
    }
    
}
