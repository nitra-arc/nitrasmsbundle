<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\Processor;

use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendSuccessInterface;

/**
 * ProcessorInterface
 * сервис отправки sms-сообщений
 */
interface ProcessorInterface
{
    
    /**
     * получить баланс
     * @return string - баланс в виде строки
     *          false - ошибка
     */
    public function getBalance();
    
    /**
     * Построить sms-сообщение при помощи sms-билдера 
     * @param SmsBuilderInterface       $smsBuilder     - строитель sms-сообщения
     */
    public function buildSms(SmsBuilderInterface $smsBuilder);
    
    /**
     * Получить новое sms-сообщение
     * @return SmsMessageInterface
     */
    public function getNewSms();
    
    /**
     * валидировать отправляемое SMS сообщение
     * @param SmsMessageInterface $smsMessage - отправляемое sms сообщение 
     * @return \Symfony\Component\Validator\ConstraintViolationList - список нарушений
     * @return false  - валидация пройдена успешно
     */
    public function validate(SmsMessageInterface $smsMessage);
    
    /**
     * Отпраить SMS
     * @param SmsMessageInterface $smsMessage - отправляемое sms сообщение 
     * @param boolean $addLog - флаг создания истории sms сообщений
     * @throws \Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendExceptionInterface - ошибка валидации, ошибка отправления сообшения
     * @return \Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendSuccessInterface - сообщение было успешно отправлено
     */
    public function send(SmsMessageInterface $smsMessage, $addLog = true);
    
    /**
     * Отпраить SMS try ... catch
     * @param SmsMessageInterface $smsMessage - отправляемое sms сообщение 
     * @param boolean $addLog - флаг создания истории sms сообщений
     * @return string - текст сообщения об ошибке
     * @return SmsSendSuccess - instanceof SmsSendSuccess - сообщение было успешно отправлено
     * @return SmsSendError   - instanceof SmsSendError   - ошибка отрпавления сообщения
     */
    public function trySend(SmsMessageInterface $smsMessage, $addLog = true);

    /**
     * Создать историю sms сообщений
     * @param SmsMessageInterface $smsMessage - отправляемое sms сообщение 
     * @param SmsSendSuccess $smsSend - результат ответа sms центра
     * @return SmsHistory - не сохраненная сущность истории в БД
     */
    public function addLog(SmsMessageInterface $smsMessage, SmsSendSuccessInterface $smsSend);
    
}
