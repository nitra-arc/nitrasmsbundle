<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend;

/**
 * SmsSendSuccess 
 * результат отправки Sms сообщения
 * Sms отправлено успешно 
 */
class SmsSendSuccess implements SmsSendSuccessInterface
{
    
    /**
     * @var integer $id - ID идентификатор сообщения, переданный Клиентом или назначенный Сервером
     */
    protected $id;
    
    /**
     * @var integer $cnt - количество отправленных SMS, вычисляемое как количество SMS в сообщении, умноженное на количество получателей.
     */
    protected $cnt;
    
    /**
     * @var float $cost - стоимость рассылки
     */
    protected $cost;
    
    /**
     * @var float $balance - новый баланс Клиента
     */
    protected $balance;
    
    /**
     * Конструктор ответа сервера
     * @param integer $id       - ID идентификатор сообщения, переданный Клиентом или назначенный Сервером
     * @param integer $cnt      - количество отправленных SMS, вычисляемое как количество SMS в сообщении, умноженное на количество получателей.
     * @param float   $cost     - стоимость рассылки
     * @param float   $balance  - новый баланс Клиента
     */
    public function __construct($id, $cnt, $cost, $balance)
    {
        // установить зависимости
        $this->id       = $id;
        $this->cnt      = $cnt;
        $this->cost     = $cost;
        $this->balance  = $balance;
    }
    
    /**
     * Получить ID сообщения
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Получить количество отправленных SMS
     */
    public function getCnt()
    {
        return $this->cnt;
    }
    
    /**
     * Получить стоимость рассылки
     */
    public function getCost()
    {
        return $this->cost;
    }
    
    /**
     * Получить новый баланс Клиента
     */
    public function getBalance()
    {
        return $this->balance;
    }
    
}
