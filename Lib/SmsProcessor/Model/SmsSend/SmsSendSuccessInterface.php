<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend;

/**
 * SmsSendSuccessInterface
 * результат отправки Sms сообщения
 * Sms отправлено успешно
 */
interface SmsSendSuccessInterface
{
    /**
     * Получить ID сообщения
     */
    public function getId();
    
    /**
     * Получить количество отправленных SMS
     */
    public function getCnt();
    
    /**
     * Получить стоимость рассылки
     */
    public function getCost();
    
    /**
     * Получить новый баланс Клиента
     */
    public function getBalance();

}
