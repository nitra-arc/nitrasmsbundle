<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend;

/**
 * SmsSendExceptionInterface
 *
 */
interface SmsSendExceptionInterface
{
    
    /**
     * Получить массив ошибок отправки сообщений
     * @return array
     */
    public static function getErrorMessages();
    
    /**
     * Получить текст сообщения об ошибке по коду ошибки
     * @param integer $code код ошибки
     */
    public static function getErrorByCode($code);
    
}
