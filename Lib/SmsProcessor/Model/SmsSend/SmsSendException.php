<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend;

/**
 * SmsSendException
 * Oшибка отправления Sms сообщения
 */
//class SmsSendException extends SmsSendExceptionAbstract
class SmsSendException extends \Exception implements SmsSendExceptionInterface
{
    
    /**
     * @var массив ошибок сервера
     * @link http://smsc.ru/api/http/#answer
     */
    protected static $errorMessages = array(
        1 => 'Ошибка в параметрах.',
        2 => 'Неверный логин или пароль.',
        3 => 'Недостаточно средств на счете Клиента.',
        4 => 'IP-адрес временно заблокирован из-за частых ошибок в запросах. http://smsc.ru/faq/#a99',
        5 => 'Неверный формат даты.',
        6 => 'Сообщение запрещено (по тексту или по имени отправителя).',
        7 => 'Неверный формат номера телефона.', 
        8 => 'Сообщение на указанный номер не может быть доставлено.',
        9 => 'Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты.',
    );
    
    /**
     * {@inheritdoc}
     */
    public static function getErrorMessages()
    {
        return self::$errorMessages;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getErrorByCode($code)
    {
        // проверить наличие текста ошибки по коду ошибки
        $errorMessages = self::getErrorMessages();
        if (!isset($errorMessages[$code])) {
            // получен не существующий код
            throw new \LogicException('Указанный код ошибки "' . $code . '" не найден.');
        }
        
        // вернуть тест ошибки
        return $errorMessages[$code];
    }
    
}
