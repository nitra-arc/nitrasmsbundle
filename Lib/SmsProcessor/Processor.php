<?php

namespace Nitra\SMSBundle\Lib\SmsProcessor;

use Nitra\SMSBundle\Lib\SmsFactory\Builders\SmsBuilderNew;
use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendException;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendSuccess;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendSuccessInterface;

/**
 * Processor
 * Сервис отправки SMS
 * @link http://smsc.ru/api
 */
class Processor extends Model\Processor\ProcessorAbstract
{
    
    /**
     * {@inheritdoc}
     */
    public function getBalance()
    {
        // получить балланс
        $result = $this->apiSendRequest('balance');
        
        // проверить балланс
        if (!isset($result['balance'])) {
            // ошибка
            return false;
        }
        
        // вернуть баланс
        return $result['balance'];
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildSms(SmsBuilderInterface $smsBuilder)
    {
        // получить sms-сообщение
        return $this->getSmsFactory()->buildSms($smsBuilder);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNewSms()
    {
        // получить строитель sms-сообщений
        $smsFactory = $this->getSmsFactory();
        
        // построить новое sms-сообшение
        $smsBuilder = new SmsBuilderNew($smsFactory->getNewSms(), $this->getSmsParameters());
        return $smsFactory->buildSms($smsBuilder);
    }
    
    /**
     * {@inheritdoc}
     */
    public function validate(SmsMessageInterface $smsMessage)
    {
        // валидировать 
        $errors = $this->getValidator()->validate($smsMessage);
        if ($errors->count()) {
            // вернуть результат валидации
            return $errors;
        }
        
        // валидация пройдена успешно
        return false;        
    }
    
    /**
     * {@inheritdoc}
     */
    public function send(SmsMessageInterface $smsMessage, $addLog = true)
    {
        
        // валидировать sms-сообшение
        $errors = $this->validate($smsMessage);
        if ($errors !== false) {
            $errorMessage = '';
            foreach($errors as $error) {
                $errorMessage .= "Ошибка валидации SMS-сообщения. Параметр: \"".$error->getPropertyPath()."\". ". $error->getMessage()."\n";
            }
            // выбросить ошибку
            throw new SmsSendException($errorMessage);
        }
        
        // сообщение отрпалется только в разрешенных окружениях 
        // проверить текущее окружение
        // если текущее окружение не разрешено для отправки сообщений
        // эмулирование отправку sms-сообщения
        if (!in_array($this->getSmsParameters()->get('environment'), $this->getSmsParameters()->getAllowEnvironments())) {
            // эмулирование отправки сообщения
            // сообщение отправлено успешно
            $smsSend = new SmsSendSuccess($smsMessage->getId(), 1, 0.15, 100);
            
            // проверить флаг создания истории sms сообщений
            if ($addLog) {
                // создать историю sms
                $this->addLog($smsMessage, $smsSend);
            }
            
            // вернуть результат отправки сообщения
            return $smsSend;
        }
        
        // массив параметров отправки sms-сообщения
        $apiOptions = $this->getSmsFactory()->buildSmsOptions($smsMessage);
        
        // Отправить sms-сообщение 
        // через sms центр
        $result = $this->apiSendRequest('send', $apiOptions);
        
        // проверить ответ сервера
        if (isset($result['error_code'])) {
            $errorMessage = 'Ошибка. Сервер '.$this->getSmsParameters()->get('host').' '
                . 'вернул код ошибки: '.$result['error_code'].'. '
                . SmsSendException::getErrorByCode($result['error_code']);
            throw new SmsSendException($errorMessage);
        }
        
        // sms-сообщение было успешно отправлено
        // создать результат отправки сообщения
        $smsSend = new SmsSendSuccess($result['id'], $result['cnt'], $result['cost'], $result['balance']);
        
        // проверить флаг создания истории sms сообщений
        if ($addLog) {
            // создать историю sms
            $this->addLog($smsMessage, $smsSend);
        }
        
        // вернуть результат отправки сообщения
        return $smsSend;
    }
    
    /**
     * {@inheritdoc}
     */
    public function trySend(SmsMessageInterface $smsMessage, $addLog = true)
    {
        
        // попытка отправить сообщение
        try {
            
            // обработать цепочку
            $result = $this->send($smsMessage);
            
        } catch (\SmsSendException $e) {
            
            // вернуть текст ошибки 
            return $e;
//            return new SmsSendException($e->getMessage());
        }
        
        // цепочка обработана успешно
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addLog(SmsMessageInterface $smsMessage, SmsSendSuccessInterface $smsSend)
    {
        // получить класс реализующий логирование sms-сообщений SmsSetterInterface
        $smsClass = $this->getSmsParameters()->getEntity('sms');
        
        // создать лог
        $sms = new $smsClass();
        $sms
            ->setPhone(implode(',', $smsMessage->getPhones()))
            ->setMessage($smsMessage->getMessage())
            ->setCost($smsSend->getCost());
        
        // persist
        $this->getEntityManager()->persist($sms);
        
        // вернуть sms
        return $sms;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function apiSendRequest($command, array $options = null)
    {
        
        // протокол доступа к sms центру
        $https = ($this->getSmsParameters()->get('use_https'))
            ? 'https://' 
            : 'http://';
        
        // сформировать url запроса
        $apiUrl = $https 
            . $this->getSmsParameters()->get('host') . '/sys/' . $command . '.php'
            . '?fmt=3' // ответ в json формате
            . '&login='     . urlencode($this->getSmsParameters()->get('login'))
            . '&psw='       . urlencode($this->getSmsParameters()->get('password'))
            . '&charset='   . $this->getSmsParameters()->get('charset')
            . (($options) ? '&' . http_build_query($options) : '')
        ;

//        // ссылка команды передаваемая в SMS центр
//        print '<pre>'; print_r($apiUrl); print '</pre>';
//        echo "<a href=\"" . $apiUrl . "\" target=\"_blank\">" . $this->getSmsParameters()->get('host') . "</a>";
//        print '<pre>'; print_r($options); print '</pre>';
//        die('<br/>line:'.__LINE__.'<br/>file: '.__FILE__);
        
        // отправить запрос на сервер 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        
        // отправить запрос в SMS центр
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        
        // вернуть ответ sms сервера
        return json_decode($apiResponse, true);        
    }

}
