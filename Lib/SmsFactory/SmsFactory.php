<?php

namespace Nitra\SMSBundle\Lib\SmsFactory;

use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessage;
use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;
use Nitra\SMSBundle\Lib\SmsProcessor\Model\SmsSend\SmsSendException;

/**
 * SmsFactory
 * Усправлюящий стоительстовм sms-сообщений
 */
class SmsFactory extends Model\SmsFactory\SmsFactoryAbstract
{
    
    /**
     * Конкретный строитель для sms-сообщения
     * @var SmsBuilderInterface $smsBuilder
     */
    protected $smsBuilder;
    
    /**
     * {@inheritdoc}
     */
    public function getNewSms()
    {
        return new SmsMessage();
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildSms(SmsBuilderInterface $smsBuilder)
    {
        // получить sms-сообщение
        return $this->setBuilder($smsBuilder)
            ->build()
            ->getResult();
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildSmsOptions(SmsMessageInterface $smsMessage)
    {
        
        // Построить массив отправки сообщения из массив доп. параметров 
        $apiOptions = array_merge($smsMessage->getOptions(), array(
            'cost'      => $smsMessage->getCost(),
            'phones'    => implode(',', $smsMessage->getPhones()),
            'mes'       => $smsMessage->getMessage(),
            'translit'  => $smsMessage->getTranslit(),
            'id'        => $smsMessage->getId(),
        ));
        
        // имя отправителя (Sender ID). 
        if ($smsMessage->getSender()) {
            $apiOptions['sender'] = $smsMessage->getSender();
        }
        
        // необходимое время доставки
        if ($smsMessage->getTime()) {
            $apiOptions['time'] = $smsMessage->getTime();
        }
        
        // формат sms-сообщения
        if ($smsMessage->getFormat()) {
            
            // получить форматы sms сообщений
            $formats = $this->getSmsParameters()->getFormats();
            
            // проверить формат сообщения
            if (!isset($formats[$smsMessage->getFormat()])) {
                $errorMessage = 'Указан не известный формат сообщения. Доступные форматы: array(';
                foreach($formats as $formatKey => $formatValue) {
                    $errorMessage .= $formatKey .'=>"'.$formatValue.'", ';
                }
                $errorMessage.= ').';
                // выбросить ошибку
                throw new SmsSendException($errorMessage);
            }
            
            // получить формат
            $format = explode('=', $formats[$smsMessage->getFormat()]);
            $apiOptions[$format[0]] = $format[1];
        }
        
        // вернуть массив параметров sms-сообщения
        return $apiOptions;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBuilder(SmsBuilderInterface $smsBuilder)
    {
        $this->smsBuilder = $smsBuilder;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $this->smsBuilder->build();
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult()
    {
        return $this->smsBuilder->getResult();
    }

}
