<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage;

/**
 * SmsMessageInterface
 * Определение отпраляемого sms-сообщения
 */
interface SmsMessageInterface extends SmsMessageSetterInterface
{
    
    /**
     * Получить идентификатор стоимости
     * @return integer стоимость  (1 или 3)
     */
    public function getCost();
    
    /**
     * Получить массив телефонов на которые отправляется sms-сообщение
     * @return array
     */
    public function getPhones();
    
    /**
     * Получить текст sms-сообщения 
     * @return string текст sms-сообщения 
     */
    public function getMessage();
    
    /**
     * переводить или нет в транслит 
     * (1,2 или 0)
     * @return boolean
     */
    public function getTranslit();
    
    /**
     * Получить необходимое время доставки
     * @return string - (DDMMYYhhmm, h1-h2, 0ts, +m)
     */
    public function getTime();
    
    /**
     * Получить $id - идентификатор сообщения. 
     * Представляет собой 32-битное число в диапазоне от 1 до 2147483647.
     * @return integer
     */
    public function getId();
    
    /**
     * Получить формат сообщения 
     * (0 - обычное sms, 1 - flash-sms, 2 - wap-push, 3 - hlr, 4 - bin, 5 - bin-hex, 6 - ping-sms)
     * @return integer
     */
    public function getFormat();
    
    /**
     * Получить имя отправителя (Sender ID). 
     * @return string 
     */
    public function getSender();
    
    /**
     * Получить массив доп. параметров, добавляемых в URL-запрос ("valid=01:00&maxsms=3&tz=2")
     * @return array
     */
    public function getOptions();
    
}
