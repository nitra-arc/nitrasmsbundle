<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage;

/**
 * SmsMessageSetterInterface
 * Билдер sms-сообщения
 */
interface SmsMessageSetterInterface
{
    
    /**
     * Установить идентификатор стоимости
     * @param integer $cost - идентификатор стоимости (1 или 3)
     */
    public function setCost($cost);

    /**
     * Установить массив телефонов на которые будет отправлено сообщение
     * телефоны через запятую или точку с запятой или массив телефонов
     * @param string|array $phones - массив|строка телефонов
     */
    public function setPhones($phones = array());
    
    /**
     * Установить текст отправляемого sms-сообщения
     * @param string $message - текст sms-сообщения
     */
    public function setMessage($message = '');
    
    /**
     * Установить переводить или нет в транслит (1,2 или 0)
     * {@inheritdoc}
     * @param integer $translit транслит
     */
    public function setTranslit($translit = 0);
    
    /**
     * Установить необходимое время доставки - (DDMMYYhhmm, h1-h2, 0ts, +m)
     * @param string $time - время доставки
     */
    public function setTime($time = 0);
    
    /**
     * Установить идентификатор сообщения. Представляет собой 32-битное число в диапазоне от 1 до 2147483647.
     * @param integer $id - идентификатор сообщения.
     */
    public function setId($id = 0);
    
    /**
     * Установить формат сообщения, (0 - обычное sms, 1 - flash-sms, 2 - wap-push, 3 - hlr, 4 - bin, 5 - bin-hex, 6 - ping-sms)
     * @param integer $format - формат сообщения
     */
    public function setFormat($format = 0);
    
    /**
     * Установить имя отправителя (Sender ID). Для отключения Sender ID по умолчанию необходимо в качестве имени передать пустую строку или точку
     * @param string $sender - имя отправителя 
     */
    public function setSender($sender = '');
    
    /**
     * Установить массив доп. параметров, добавляемых в URL-запрос ("valid=01:00&maxsms=3&tz=2")
     * @param array $options - массив доп. параметров
     */
    public function setOptions(array $options = array());
    
}
