<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage;

use Doctrine\Common\Inflector\Inflector;

/**
 * SmsMessageDefault
 * Установка данных sms-сообщения по умолчанию 
 */
class SmsMessageDefault implements SmsMessageDefaultInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function setDefaultData()
    {
        // утсановить данные из массива
        $this->setArray(array(
            'cost'      => 3, // ответ в виде (id, cnt, cost, balance)
            'phones'    => array(),
            'message'   => '',
            'translit'  => 0,
            'time'      => date('dmyHi'), // (DDMMYYhhmm, h1-h2, 0ts, +m)
            'id'        => 0,
            'format'    => 2,
            'sender'    => '',
            'options'   => array(),
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function setArray(array $data)
    {
        // обойти массив устанваливаемых данных 
        foreach ($data as $k => $v) {
            // получить имя медота setter
            $methodName = Inflector::camelize('set_'.$k);
            // метод setter не найден
            if (!method_exists($this, $methodName)) {
                throw new \LogicException('Для класса: "'.get_class($this).'" метод "'.$methodName.'" не существует.');
            }
            // установить свойтво
            $this->$methodName($v);
        }
        // вернуть sms-сообщение
        return $this;        
    }
    
}
