<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage;

/**
 * SmsMessageDefaultInterface
 * 
 */
interface SmsMessageDefaultInterface
{
    /**
     * Установить данные сообщения по умолчанию
     */
    public function setDefaultData();
    
    /**
     * Установить свойства сообщения из массива
     * @param   array $data массив of key => value pairs
     */
    public function setArray(array $data);
    
}
