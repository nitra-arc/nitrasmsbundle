<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage;

use Symfony\Component\Validator\Constraints as Assert;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageDefault;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;

/**
 * SMS данные сообщения 
 * передаваемое в SMS центр
 * SmsMessage
 */
class SmsMessage extends SmsMessageDefault implements SmsMessageInterface
{

    /**
     * @var integer $cost - идентификатор стоимости
     * (1 или 3)
     */
    protected $cost;
    
    /**
     * @var array $phones - массив телефонов
     * @Assert\NotBlank(message="Не указан телефон.")
     */
    protected $phones;
    
    /**
     * @var string $message - текст сообщения
     * @Assert\NotBlank(message="Не указан текст сообщения.")
     */
    protected $message;
    
    /**
     * @var integer $translit - переводить или нет в транслит 
     * (1,2 или 0)
     */
    protected $translit;
    
    /**
     * @var string $time - необходимое время доставки 
     * в виде строки (DDMMYYhhmm, h1-h2, 0ts, +m)
     */
    protected $time;
    
    /**
     * @var integer $id - идентификатор сообщения. 
     * Представляет собой 32-битное число в диапазоне от 1 до 2147483647.
     */
    protected $id;
    
    /**
     * @var integer $format - формат сообщения 
     * (0 - обычное sms, 1 - flash-sms, 2 - wap-push, 3 - hlr, 4 - bin, 5 - bin-hex, 6 - ping-sms)
     */
    protected $format; 
    
    /**
     * @var string $sender - имя отправителя (Sender ID). 
     * Для отключения Sender ID по умолчанию необходимо в качестве имени передать пустую строку или точку
     * @Assert\NotBlank(message="Не указан отправитель сообщения.")
     */
    protected $sender;
    
    /**
     * @var array - $options  - массив доп. параметров, добавляемых в URL-запрос ("valid=01:00&maxsms=3&tz=2")
     */
    protected $options;
    
    /**
     * Конструктор
     */
    public function __construct() {
        // установить данные сообщения по умолчанию
        $this->setDefaultData();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCost()
    {
        return $this->cost;
    }
    
    /**
     * Установить идентификатор стоимости
     * @param integer $cost - идентификатор стоимости (1 или 3)
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function getPhones()
    {
        return $this->phones;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setPhones($phones = array())
    {
        // тетефон в виде стринга
        if (is_string($phones)) {
            // преобразовать в массив телефонов
            // без повторющизся номеров
            $this->phones = array_unique(preg_split('/[\s,;]/', $phones, null, PREG_SPLIT_NO_EMPTY));
        }
        
        // телефоны в виде массива
        if (is_array($phones)) {
            // без повторющизся номеров
            $this->phones = array_unique($phones);
        }
        
        // вернутьт sms-сообщение
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setMessage($message = '')
    {
        $this->message = $message;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTranslit()
    {
        return $this->translit;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setTranslit($translit = 0)
    {
        $this->translit = ($translit) ? 1 : 0;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTime()
    {
        return $this->time;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setTime($time = 0)
    {
        $this->time = $time;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setId($id = 0)
    {
        $this->id = $id;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormat()
    {
        return $this->format;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setFormat($format = 0)
    {
        $this->format = $format;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSender()
    {
        return $this->sender;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setSender($sender = '')
    {
        $this->sender = $sender;
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getOptions()
    {
        return $this->options;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setOptions(array $options = array())
    {
        $this->options = $options;
        return $this;
    }

}
