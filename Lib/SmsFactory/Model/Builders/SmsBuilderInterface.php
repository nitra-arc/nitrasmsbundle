<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\Builders;

use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersInterface;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersAwareInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;

/**
 * SmsBuilderInterface
 * Строитель sms-сообшения
 */
interface SmsBuilderInterface extends SmsParametersAwareInterface
{
    
    /**
     * Конструктор строителя sms-сообщений
     * @param SmsMessageInterface       $smsMessage     - sms-сообщение
     * @param SmsParametersInterface    $smsParameters  - параметры sms-сообщений
     */
    public function __construct(SmsMessageInterface $smsMessage, SmsParametersInterface $smsParameters);
    
    /**
     * Построить sms-сообщение
     * @return SmsBuilderInterface 
     */
    public function build();
    
    /**
     * Получить sms-сообщение
     * результат строительства
     * @return \Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface
     */
    public function getResult();

}
