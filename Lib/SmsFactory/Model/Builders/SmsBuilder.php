<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\Builders;

use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersAware;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;

/**
 * SmsBuilder
 * общий строитель sms-сообщений
 */
abstract class SmsBuilder extends SmsParametersAware implements SmsBuilderInterface
{

    /**
     * sms-сообщение
     * @var \Nitra\SMSBundle\Lib\SmsBuilder\Model\SmsMessage\SmsMessageInterface
     */
    protected $smsMessage;
    
    /**
     * {@inheritdoc}
     */
    abstract public function build();
    
    /**
     * {@inheritdoc}
     */
    abstract public function getResult();

    /**
     * {@inheritdoc}
     */
    public function __construct(SmsMessageInterface $smsMessage, SmsParametersInterface $smsParameters)
    {
        // уствновить зависимости
        $this->smsParameters    = $smsParameters;
        $this->smsMessage       = $smsMessage;
    }
    
}
