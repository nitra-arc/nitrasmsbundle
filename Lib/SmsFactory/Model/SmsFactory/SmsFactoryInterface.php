<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsFactory;

use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;

/**
 * SmsFactoryInterface
 * описание управляющего строительством скласса
 */
interface SmsFactoryInterface
{
    
    /**
     * Получить новое sms-сообщение
     * @return \Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface sms-сообщение
     */
    public function getNewSms();

    /**
     * Построить sms-сообщение при помощи sms-билдера 
     * @param SmsBuilderInterface $smsBuilder конкретный строитель sms-сообщения
     */
    public function buildSms(SmsBuilderInterface $smsBuilder);
    
    /**
     * Построить массив параметров sms-сообщения, передаваемых на сервер SMS центра
     * @param SmsMessageInterface $smsMessage - sms-сообщение 
     * @return array 
     */
    public function buildSmsOptions(SmsMessageInterface $smsMessage);
    
    /**
     * Установить конкретный строитель для sms-сообщения
     * @param SmsBuilderInterface $smsBuilder конкретный строитель sms-сообщения
     * @return SmsFactoryInterface 
     */
    public function setBuilder(SmsBuilderInterface $smsBuilder);
    
    /**
     * Построить sms-сообщение
     * @return SmsFactoryInterface 
     */
    public function build();
    
    /**
     * Получить результат строительства
     * @return результат конкретного строителя
     */
    public function getResult();
    
}
