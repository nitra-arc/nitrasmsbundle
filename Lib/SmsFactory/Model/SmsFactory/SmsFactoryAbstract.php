<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Model\SmsFactory;

use Nitra\SMSBundle\Lib\SmsParameters\SmsParameters;
use Nitra\SMSBundle\Lib\SmsParameters\SmsParametersAware;
use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilderInterface;
use Nitra\SMSBundle\Lib\SmsFactory\Model\SmsMessage\SmsMessageInterface;

/**
 * SmsFactoryAbstract
 */
abstract class SmsFactoryAbstract extends SmsParametersAware implements SmsFactoryInterface
{

    /**
     * {@inheritdoc}
     */
    abstract public function getNewSms();
    
    /**
     * {@inheritdoc}
     */
    abstract public function buildSms(SmsBuilderInterface $smsBuilder);
    
    /**
     * {@inheritdoc}
     */
    abstract public function buildSmsOptions(SmsMessageInterface $smsMessage);
    
    /**
     * {@inheritdoc}
     */
    abstract public function setBuilder(SmsBuilderInterface $builder);
    
    /**
     * {@inheritdoc}
     */
    abstract public function build();

    /**
     * {@inheritdoc}
     */
    abstract public function getResult();
    
    /**
     * Конструктор
     * @param SmsParameters $smsParameters параметры sms-сообщений
     */
    public function __construct(SmsParameters $smsParameters)
    {
        // установить зависимости
        $this->smsParameters = $smsParameters;
    }

}
