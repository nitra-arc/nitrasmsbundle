<?php

namespace Nitra\SMSBundle\Lib\SmsFactory\Builders;

use Nitra\SMSBundle\Lib\SmsFactory\Model\Builders\SmsBuilder;

/**
 * SmsBuilderNew
 * Строитель нового сообщения
 */
class SmsBuilderNew extends SmsBuilder 
{
    
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        // получить параметры sms-сообщения
        $smsParameters = $this->getSmsParameters();
        
        // построить sms-сообщение 
        $this->smsMessage->setSender($smsParameters->get('sender'));
    }

    /**
     * {@inheritdoc}
     */
    public function getResult()
    {
        // вернуть результат строительсва
        return $this->smsMessage;
    }

}
