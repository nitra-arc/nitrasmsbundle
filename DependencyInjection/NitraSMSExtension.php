<?php

namespace Nitra\SMSBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Doctrine\Common\Inflector\Inflector;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NitraSMSExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // получить настройки
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        // загрузить сервисы
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        // установить параметры sms-сообщений
        $definition = $container->getDefinition('nitra.sms.parameters');
        $definition->addMethodCall('set', array('environment',  $container->getParameter('kernel.environment')));
        $definition->addMethodCall('set', array('allow_environments',   $config['allow_environments']));
        $definition->addMethodCall('set', array('login',                $config['login']));
        $definition->addMethodCall('set', array('password',             $config['password']));
        $definition->addMethodCall('set', array('sender',               $config['sender']));
        $definition->addMethodCall('set', array('use_https',            $config['use_https']));
        $definition->addMethodCall('set', array('host',                 $config['host']));
        $definition->addMethodCall('set', array('charset',              $config['charset']));
        $definition->addMethodCall('set', array('formats',              $config['formats']));
        $definition->addMethodCall('set', array('entity',               $config['entity']));
    }
    
    /**
     * prepend
     */
    public function prepend(ContainerBuilder $container)
    {
        // Изменить настройки doctrine ORM
        $this->prependORM($container);
    }

    /**
     * Изменить настройки doctrine ORM
     */
    protected function prependORM(ContainerBuilder $container)
    {
        
        // если не установлена doctrine
        if (!$container->hasExtension('doctrine')) {
            // прервать выполнение
            return;
        }
        
        // получить конфигурацию бандла
        $extensionConfig = $container->getExtensionConfig('nitra_sms');
        
        // провеверить если в config.yml НЕ добавлены настройки
        // прерываем выполнение
        if (!isset($extensionConfig[0]['entity'])) {
            return;
        }
        
        // массив интерфейсов сущностей
        $resolveTargetEntities = array(
            'orm' => array(
                'resolve_target_entities' => array()
        ));
        
        // обойти все сушности банлда
        foreach($extensionConfig[0]['entity'] as $modelName => $modelClass) {
            // интерфейс сущности
            $interfaceName = 'Nitra\\SMSBundle\\Entity\Model\\' . Inflector::classify($modelName) . 'Interface';
            $resolveTargetEntities['orm']['resolve_target_entities'][$interfaceName] = $modelClass;
            
            // создать параметр сущности
            $parameterName = 'nitra_sms.entity.'.$modelName;
            $container->setParameter($parameterName, $modelClass);
        }
        
        // создать параметры интерфейсов реализующих сушности
        $container->setParameter('nitra_sms.resolve_target_entities', $resolveTargetEntities['orm']['resolve_target_entities']);
        
        // добавить массив интерефейсов в настройки doctrine
        $container->prependExtensionConfig('doctrine', $resolveTargetEntities);
    }    
}
