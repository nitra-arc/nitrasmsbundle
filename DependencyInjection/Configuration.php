<?php
namespace Nitra\SMSBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nitra_sms')->addDefaultsIfNotSet();
        
        // добавить настройки sms центра
        $this->addSmsCenter($rootNode);
        
        // добавить настройки отправки sms форматов 
        $this->addFormats($rootNode);
        
        // добавить настройки разрешенных окружений
        $this->addAllowEnvironments($rootNode);
        
        // добавить настройки сущностей
        $this->addEntity($rootNode);
        
        // вернуть дерево натсроек
        return $treeBuilder;
    }
    
    /**
     * Добавить настройки sms центра
     */
    private function addSmsCenter(ArrayNodeDefinition $builder)
    {
        // добавить настроки отправки sms сообщений 
        $builder
            ->addDefaultsIfNotSet()
            ->children()
            
                // логин пользователя sms центра
                ->scalarNode('login')->isRequired()->cannotBeEmpty()->end()
                
                // пароль пользователя sms центра
                ->scalarNode('password')->isRequired()->cannotBeEmpty()->end()
            
                // имя отправителя 
                ->scalarNode('sender')->isRequired()->cannotBeEmpty()->end()
            
                // передача sms сообщения по https
                ->booleanNode('use_https')->defaultValue(false)->end()
            
                // хост sms-центра
                ->scalarNode('host')->defaultValue('smsc.ru')->end()
                
                // кодировка sms-сообщения
                ->scalarNode('charset')->defaultValue('utf-8')->end()
            
            ->end();
    }    
    
    /**
     * Добавить настройки форматов sms сообщений
     */
    private function addFormats(ArrayNodeDefinition $builder)
    {
        // массив форматов
        $formats = array(1 => 'flash=1', 'push=1', 'hlr=1', 'bin=1', 'bin=2', 'ping=1');
        
        // добавить настроки 
        $builder
            ->addDefaultsIfNotSet()
            ->children()
                ->variableNode('formats')->defaultValue($formats)->end()
            ->end();
    }
    
    /**
     * Добавить настройки разрешенных окружений
     */
    private function addAllowEnvironments(ArrayNodeDefinition $builder)
    {
        // массив окружений 
        $environments = array('prod');
        
        // добавить настроки 
        $builder
            ->addDefaultsIfNotSet()
            ->children()
                ->variableNode('allow_environments')->defaultValue($environments)->end()
            ->end();
    }
    
    /**
     * добавить настройки сущностей
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addEntity(ArrayNodeDefinition $treeBuilder)
    {
        // установить конфигурационные данные
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('entity')->isRequired()->cannotBeEmpty()
                ->addDefaultsIfNotSet()
                    ->children()
                        
                        // сущность прайса
                        ->scalarNode('sms')
                            ->example('Nitra\\TetradkaSMSBundle\\Entity\\Sms')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                        
                    ->end()
                ->end()
            ->end();        
    }    
    
}
