# NitraSMSBundle #

Отправка  SMS-сообщений через SMS-центр www.smsc.ru.

*****

## [Подключение](#markdown-header-_2) ##

* [composer.json](#markdown-header-composerjson)
* [app/AppKernel.php](#markdown-header-appappkernelphp)
* [app/config/routing.yml](#markdown-header-appconfigroutingyml)

*****

## [Конфигурация](#markdown-header-_3) ##

* [app/config/parameters.yml.dist](#markdown-header-appconfigparametersymldist)
* [app/config/config.yml](#markdown-header-appconfigconfigyml)

*****

## [Пример отправки](#markdown-header-_5) ##

* [NewController.php](#markdown-header-newcontrollerphp)

*****

### Подключение ###

#### composer.json ####
```json
{
    ...
    "require": {
        ...
        "nitra/sms-bundle": "dev-master",
        ...
    }
    ...
}
```

#### app/AppKernel.php ####
```php
<?php
//...
class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            // NitraSMSBundle
            new Nitra\SMSBundle\NitraSMSBundle(),
            new Nitra\ExtensionsAdminBundle\NitraExtensionsAdminBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

####  app/config/routing.yml ####
```yml
...
NitraSMSBundle:
    resource: "@NitraSMSBundle/Resources/config/routing.yml"
    prefix:   /
...

```


*****


### Конфигурация ###

#### app/config/parameters.yml.dist ####
Конфигурация вынесена в файл parameters.yml в app/config проекта.
```yml
...
    nitra_sms_login:    "login"
    nitra_sms_password: "password"
...

```

#### app/config/config.yml ####
Конфигурация по умолчанию файл config.yml в app/config проекта.
```yml
...
nitra_sms:

    # авторизация 
    login:      "%nitra_sms_login%"
    password:   "%nitra_sms_password%"
    use_https:  false

    # Имя отправителя sms-сообщения
    sender:     "sms-center-sender"

    # хост
    host:       "smsc.ru"
    charset:    "utf-8"

    # sms-сообщения будут оправляться только в указанных окружениях
    # для prod - будет отправлено sms-сообщение и будет создан лог отпраки
    # для dev  - будет создан тлько лог отправки
    allow_environments:
        - "prod"

    # массив сущностей с которыми работает бандл
    entity:
        sms:    "Nitra\\TetradkaSMSBundle\\Entity\\Sms"

    # форматы sms-сообщений
    formats:
        1:      "flash=1"
        2:      "push=1"
        3:      "hlr=1"
        4:      "bin=1"
        5:      "bin=2"
        6:      "ping=1"

...

```


*****


### Пример отправки ###

#### NewController.php ####
Admingenerated\NitraSMSBundle\BaseSmsController\NewController.php
```php
<?php
// ...

    /**
     * Получить сервис отправки sms-сообщений
     * @return \Nitra\SMSBundle\Lib\SmsProcessor\Processor
     */
    public function getNitraSms()
    {
        return $this->get('nitra.sms');
    }
    
    /**
     * Сохранить новую запись в БД
     * @param \Nitra\MiniTetradkaBundle\Entity\Sms $Sms
     */
    protected function saveObject(\Nitra\TetradkaSMSBundle\Entity\Sms $Sms)
    {
        // получить sms-сообщение
        $smsMessage = $this->getNitraSms()->getNewSms()
            ->setPhones($Sms->getPhone())
            ->setMessage($Sms->getMessage());
        
        // отправить sms сообщение
        // отключаем логирование потому что создаем лог вручную текущим контроллером
        $smsSend = $this->getNitraSms()->send($smsMessage, false);
        
        // сохранить лог родителем 
        parent::saveObject($Sms);
    }

// ...
```

